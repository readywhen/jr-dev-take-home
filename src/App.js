import React from 'react';
import logo from './logo.svg';
import expected from './expected.png';
import { Counter } from './features/counter/Counter';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <Counter />
        <p>
          TODOs:
          <ol style={{textAlign: 'left'}}>
            <li>Make a SideBar Component that is hidden by default</li>
            <li>Add a button that can toggle the sidebar to slide open and close</li>
            <li>Move Add amount input and button to the sidebar</li>
            <li>Update main layout to match the screenshot below</li>
          </ol>
        </p>

        <p>Expected result:</p>
        <img src={expected} width="630"/>
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
      </header>
    </div>
  );
}

export default App;
